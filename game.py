
import random

name = input( "What is your name? ")

loop = [1, 2, 3]

for i in loop:
    month = random.randint(1,12)
    year = random.randint(1920,2022)
    answer = input(f"Hi, {name} were you born {month}/{year}? ")
    
    if answer.lower() == "yes":
        print('I knew  it!')
        exit()
    elif i == 3:
        print("We tried guessing 3 times and failed.")
    elif answer.lower() == 'no':
        print("Drat! Lemme try again.")
    elif answer.lower() != 'no' or 'yes':
        print("Please answer yes or no.")